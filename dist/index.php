<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <title>Atividade Proposta SASS</title>
</head>
<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <nav id="menuTopo">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Empresa</a></li>
                        <li><a href="#">Produtos</a></li>
                        <li><a href="#">Serviços</a></li>
                        <li><a href="#">Contato</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col text-center">
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Explicabo voluptatibus, eveniet aliquam consectetur ipsum quos numquam soluta eum ipsam, minima, aliquid.</p>
                <a href="#" class="botao-primario">Cadastrar</a>
                <a href="#" class="botao-secundario">Editar</a>
            </div>
        </div>
    </div>
    <script src="js/jquery.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/app.js"></script>
</body>
</html>